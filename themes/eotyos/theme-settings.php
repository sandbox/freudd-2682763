<?php
/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function eotyos_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL)  {
  // Work-around for a core bug affecting admin themes. See issue #943212.
	if (isset($form_id)) {
		return;
	}
	
	// SCROLL MENU
	$form['scroll_menu'] = array(
		'#type'          => 'fieldset',
		'#title'         => t('Scroll Menu settings'),
	);
	$form['scroll_menu']['eotyos_scroll_menu'] = array(
		'#type'          => 'select',
		'#title'         => t('Use Scroll Menu'),
		'#default_value' => theme_get_setting('eotyos_scroll_menu'),
		'#options'       => array(
							'yes'   => t('Yes'),
							'no'    => t('No'),
                        ),
	);  
	$form['scroll_menu']['scroll_menu_options'] = array(
		'#type' => 'container',
		'#states' => array(
			'invisible' => array(
				':input[name="eotyos_scroll_menu"]' => array('value' => 'no'),
			),
		),
	);
	$form['scroll_menu']['scroll_menu_options']['eotyos_scroll_menu_class'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Scroll Menu class'),
		'#description'   => t('Text only. Do Not to include spaces.'),
		'#default_value' => theme_get_setting('eotyos_scroll_menu_class'),
		'#element_validate' => array('element_validate_isset'),
	);
	$form['scroll_menu']['scroll_menu_options']['eotyos_scroll_menu_speed'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Speed Scroll Menu class'),
		'#description'   => t('Number only. Do Not to include spaces.'),
		'#default_value' => theme_get_setting('eotyos_scroll_menu_speed'),
		'#element_validate' => array('element_validate_integer_positive'),
	);

  // Create the form using Forms API: http://api.drupal.org/api/7

  /* -- Delete this line if you want to use this setting
  $form['eotyos_example'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('eotyos sample setting'),
    '#default_value' => theme_get_setting('eotyos_example'),
    '#description'   => t("This option doesn't do anything; it's just an example."),
  );
  // */

  // Remove some of the base theme's settings.
  /* -- Delete this line if you want to turn off this setting.
  unset($form['themedev']['zen_wireframes']); // We don't need to toggle wireframes on this site.
  // */

  // We are editing the $form in place, so we don't need to return anything.
}

/**
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function element_validate_isset($element, &$form_state) {
  $value = $element['#value'];
  if (ctype_alpha($value)) {
    form_error($element, t('%name is null.', array('%name' => $element['#title'])));
  }
}