<?php
/**
 * @file
 * openeportfolio_project.features.inc
 */

/**
 * Implements hook_views_api().
 */
function openeportfolio_project_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function openeportfolio_project_node_info() {
  $items = array(
    'project' => array(
      'name' => t('Project'),
      'base' => 'node_content',
      'description' => t('Ajoutez vos projets (personnels, professionnels, universitaires ...)'),
      'has_title' => '1',
      'title_label' => t('Nom'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
