<?php
/**
 * @file
 * openeportfolio_captcha.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function openeportfolio_captcha_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "captcha" && $api == "captcha") {
    return array("version" => "1");
  }
}
