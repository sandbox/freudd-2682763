<?php
/**
 * @file
 * openeportfolio_skills_demo.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function openeportfolio_skills_demo_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Strategic Planning',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 20,
    'uuid' => '0c0c2755-c1a8-46aa-8ef2-4855b6f53de2',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/strategic-planning',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Management',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 11,
    'uuid' => '0eb68aec-f893-4a75-96e5-d0785e6c2243',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/management',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Photoshop',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 14,
    'uuid' => '27616eeb-e2fa-488b-9ba8-3184ba24599b',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/photoshop',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Project Management',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 15,
    'uuid' => '27d6017b-ea15-4efa-a35d-0e783143bbdb',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/project-management',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Marketing',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 12,
    'uuid' => '2d19f9ad-8bf2-41d1-b119-762fa7e01f03',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/marketing',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Customer Service',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 8,
    'uuid' => '3865d44f-6c66-4f19-9fff-a21d18e6f691',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/customer-service',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Microsoft PowerPoint',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '3ce39c8e-8f66-4033-8876-5252ffd45cd5',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(
      'und' => array(
        0 => array(
          'value' => 70,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/microsoft-powerpoint',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'e0c9fa6c-9a1f-439f-99f0-74f9dab0c758',
    ),
  );
  $terms[] = array(
    'name' => 'English',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 9,
    'uuid' => '400e6f00-fbca-4fbe-a720-97e94ac889fc',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/english',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Team Leadership',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 22,
    'uuid' => '48095ee8-bb55-42e4-9407-cf8d8d8ccb76',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/team-leadership',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Change Management',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 4,
    'uuid' => '50077e43-b14c-4912-b7fa-a5b2fd278ac3',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/change-management',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Marketing Communication',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '58fd64a0-b994-481b-860d-b2bc8c3dc8aa',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(
      'und' => array(
        0 => array(
          'value' => 85,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/marketing-communication',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => '2d19f9ad-8bf2-41d1-b119-762fa7e01f03',
    ),
  );
  $terms[] = array(
    'name' => 'Teamwork',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 23,
    'uuid' => '5fc8ff27-51bb-402b-a98d-2af7347c83ad',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/teamwork',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Research',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 18,
    'uuid' => '85845842-3557-43b3-80ab-b5e558d5184a',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/research',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Microsoft Excel',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '8cf6b3a3-ec3c-4319-b414-84bfcb8a94c5',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(
      'und' => array(
        0 => array(
          'value' => 60,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/microsoft-excel',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'e0c9fa6c-9a1f-439f-99f0-74f9dab0c758',
    ),
  );
  $terms[] = array(
    'name' => 'Training',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 24,
    'uuid' => '8f96cdb4-5349-4483-ab2a-33779f4747fe',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/training',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Communication',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 7,
    'uuid' => '93db89b4-72a5-428e-8b6f-fba654473e88',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/communication',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Microsoft Word',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => 'b6a0004e-e73c-4989-b74a-7efbbae005ed',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(
      'und' => array(
        0 => array(
          'value' => 90,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/microsoft-word',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => 'e0c9fa6c-9a1f-439f-99f0-74f9dab0c758',
    ),
  );
  $terms[] = array(
    'name' => 'Social Media',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 19,
    'uuid' => 'c5b79deb-ed27-40ad-a71e-446a3f4f8291',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/social-media',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Teaching',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 21,
    'uuid' => 'd5db5712-a3ca-44bd-97e4-7eb9cbacecf2',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/teaching',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Public Relations',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 16,
    'uuid' => 'd8862d10-097f-44e7-a03f-d55df117f845',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/public-relations',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Public Speaking',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 17,
    'uuid' => 'dd5035ec-7e92-4a30-9e7f-8d9fb8321f32',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/public-speaking',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Marketing Strategy',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => 'e00c4fd0-b264-4f04-82cd-db9d0c2b9247',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(
      'und' => array(
        0 => array(
          'value' => 90,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/marketing-strategy',
        'language' => 'und',
      ),
    ),
    'parent' => array(
      0 => '2d19f9ad-8bf2-41d1-b119-762fa7e01f03',
    ),
  );
  $terms[] = array(
    'name' => 'Microsoft Office',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 13,
    'uuid' => 'e0c9fa6c-9a1f-439f-99f0-74f9dab0c758',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/microsoft-office',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Leadership',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 10,
    'uuid' => 'f2dba017-833b-4b20-8ab0-49fc0802a95a',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/leadership',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Coaching',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => 'f529723f-2c77-4039-b920-9e29bce888db',
    'vocabulary_machine_name' => 'skills',
    'field_percentage' => array(),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'competences/coaching',
        'language' => 'und',
      ),
    ),
  );
  return $terms;
}
