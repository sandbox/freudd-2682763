<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>
<div class="container-fluid" id="page">
	<div class="row">
	
		<div id="header_wrapper" class="col-md-3 col-xs-12">	
			<div id="header_content">
				<div class="row">
				
				<?php if ($page['header_top']): ?>
				<div class="col-md-12">
					<div id="header-top">
						<?php print render($page['header_top']); ?>
					</div> <!-- /#header-top -->
				</div>
				<?php endif; ?>

				<?php if ($logo): ?>
				<div class="col-md-12">
				<div id="logo">
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a>
				</div> <!-- /#logo -->
				</div>
				<?php endif; ?>

				<?php if ($site_name || $site_slogan): ?>
				<div class="col-md-12">
				<div id="name-and-slogan">
					<?php if ($site_name): ?>
						<!-- Un e-portfolio promeut une personne, donc le titre h1 du site est toujours son nom -->
						<h1 id="site-name">
							<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>						
						</h1>
					<?php endif; ?>

					<?php if ($site_slogan): ?>
					<div id="site-slogan">
						<?php print $site_slogan; ?>
					</div> <!-- /#site-slogan -->
					<?php endif; ?>
				</div> <!-- /#name-and-slogan -->
				</div>
				<?php endif; ?>
				
				<?php if ($page['header']): ?>
				<div class="col-md-12">
					<div class="header">
						<?php print render($page['header']); ?>
					</div> <!-- /.header -->
				</div>
				<?php endif; ?>
				
				<?php if ($main_menu): ?>
				<div class="col-md-12">
				<div id="main-menu" class="navigation">
					<?php print theme('links__system_main_menu', array(
					  'links' => $main_menu,
					  'attributes' => array(
						'id' => 'main-menu',
						'class' => array('links'),
					  ),
					  'heading' => array(
						'text' => t('Main menu'),
						'level' => 'h2',
						'class' => array('element-invisible'),
					  ),
					)); ?>
				</div> <!-- /#main-menu -->
				</div>
				<?php endif; ?>

				<?php if ($secondary_menu): ?>
				<div class="col-md-12">
				<div id="secondary-menu" class="navigation">
					<?php print theme('links__system_secondary_menu', array(
					  'links' => $secondary_menu,
					  'attributes' => array(
						'id' => 'secondary-menu',
						'class' => array('links', 'inline'),
					  ),
					  'heading' => array(
						'text' => t('Secondary menu'),
						'level' => 'h2',
						'class' => array('element-invisible'),
					  ),
					)); ?>
				</div> <!-- /#secondary-menu -->
				</div>
				<?php endif; ?>
			</div>
			</div> <!-- /#header_content -->
		</div> <!-- /#header_wrapper /.col-md-4 /.col-xs-12 -->
		
		<div id="main_wrapper" class="col-md-9 col-md-offset-3 col-xs-12 col-xs-offset-0">
			<div id="main_content">
				<div class="row">
				
					<div class="col-md-12">
						<div class="main_top">
							<?php print render($page['main_top']); ?>
						</div> <!-- /.main_top -->
					</div> <!-- /.col-md-12 -->
						
					<div class="col-md-12">
						<div class="main">
							<?php print render($page['highlighted']); ?>
							<?php print $breadcrumb; ?>
							<a id="main-content"></a>
							<?php print render($title_prefix); ?>
							<?php if ($title): ?>
							<h2 class="page__title title" id="page-title"><?php print $title; ?></h2>
							<?php endif; ?>
							<?php print render($title_suffix); ?>
							<?php print $messages; ?>
							<?php if ($tabs): ?>
							<?php print render($tabs); ?>
							<?php endif; ?>
							<?php print render($page['help']); ?>
							<?php if ($action_links): ?>
							<ul class="action-links"><?php print render($action_links); ?></ul>
							<?php endif; ?>
							<?php print render($page['content']); ?>
							<?php print $feed_icons; ?>
						</div> <!-- /.main -->
					</div> <!-- /.col-md-12 -->
					
					<div class="col-md-12">
						<div class="main_bottom">
							<?php print render($page['main_bottom']); ?>
						</div> <!-- /.main_bottom -->
					</div> <!-- /.col-md-12 -->

				</div> <!-- /.row -->
			</div> <!-- /#main_content -->
		</div> <!-- /#main_wrapper /.col-md-8 .col-md-offset-4 /.col-xs-12 .col-xs-offset-0 -->
		
		<div id="footer_wrapper" class="col-md-3 col-xs-12">
			<div id="footer_content">
				<?php if ($page['footer']): ?>
				<?php print render($page['footer']); ?>
				<?php endif; ?>
			</div> <!-- /#footer_content -->
		</div> <!-- /#footer_wrapper /.col-md-4 /.col-xs-12 -->
	</div> <!-- /.row -->
	
	<!-- To top -->
	<div id="btn_up">
		<img style="opacity: 0.50;" src="<?php print $GLOBALS['base_url'].'/'.$GLOBALS['theme_path']; ?>/images/fleche.png" alt="" width="40" />
	</div>
	
</div> <!-- /.container-fluid /#page -->
