<?php
/**
 * @file
 * openeportfolio_tags_demo.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function openeportfolio_tags_demo_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Lorem ipsum',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '875d6363-b8a6-44d8-9c96-46eb5048adaf',
    'vocabulary_machine_name' => 'tags',
    'url_alias' => array(
      0 => array(
        'alias' => 'etiquettes/lorem-ipsum',
        'language' => 'und',
      ),
    ),
  );
  return $terms;
}
