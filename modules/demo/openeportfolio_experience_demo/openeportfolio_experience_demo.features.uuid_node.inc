<?php
/**
 * @file
 * openeportfolio_experience_demo.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function openeportfolio_experience_demo_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 0,
  'title' => 'Communication assistant',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'vuuid' => 'a053fe14-d693-4999-99e3-e523e815d813',
  'type' => 'experience',
  'language' => 'fr',
  'created' => 1455533435,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '3a1525e0-0325-466a-91c7-4e39d53af305',
  'revision_uid' => 1,
  'field_date' => array(
    'und' => array(
      0 => array(
        'value' => '2001-01-01 00:00:00',
        'value2' => '2004-01-01 00:00:00',
        'timezone' => 'Europe/Paris',
        'timezone_db' => 'Europe/Paris',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_image' => array(),
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper lectus nec orci egestas consequat. Cras facilisis enim accumsan ligula suscipit accumsan. Fusce consectetur scelerisque odio tempor maximus. Nulla facilisi. Donec placerat porttitor accumsan. Aliquam cursus ultrices laoreet. Vivamus in pulvinar orci. Pellentesque tempus ex quis mi scelerisque, in interdum libero convallis. Ut feugiat enim neque, ut imperdiet elit consectetur id. Vivamus non magna non purus maximus euismod. Cras mollis magna et tincidunt posuere. In hac habitasse platea dictumst. Suspendisse potenti. Aliquam at enim at eros dapibus consequat vitae non purus.</p>

<p>Nullam dapibus egestas sagittis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean id auctor metus, vel ultricies risus. Quisque id semper tortor, id mollis tellus. Quisque interdum, diam in hendrerit mollis, velit dolor tristique ipsum, ac condimentum nunc enim quis nulla. Fusce dictum est vitae nunc pharetra efficitur. Morbi et tellus dignissim, tempor nisi a, efficitur purus. Nam et velit ac nisi semper finibus vitae sed tortor. Mauris luctus enim in sapien egestas dapibus. Pellentesque velit justo, gravida ut ultricies feugiat, vulputate at lectus. Vivamus est leo, suscipit eget lectus pellentesque, euismod lobortis dolor. Maecenas pretium efficitur tellus.</p>

<p>Donec eget velit tellus. Nam fermentum sem nibh, et volutpat tortor dictum sit amet. Proin id viverra eros. Mauris ullamcorper ullamcorper magna non auctor. Duis eget sem bibendum, lacinia quam sed, gravida massa. Vestibulum viverra sed tortor sed suscipit. Etiam tempor purus augue, sed egestas tellus gravida id. Pellentesque egestas massa interdum tincidunt blandit. Curabitur vehicula purus in erat malesuada sodales. Sed ut fringilla lectus. Ut non consequat tellus. Donec dictum libero quis ipsum condimentum dapibus. Curabitur accumsan nisl id est placerat, posuere ornare est consectetur. Maecenas hendrerit, massa in mollis consequat, lacus felis aliquam sem, non pharetra est libero in nibh. Etiam et dapibus neque, finibus iaculis nibh.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper lectus nec orci egestas consequat. Cras facilisis enim accumsan ligula suscipit accumsan. Fusce consectetur scelerisque odio tempor maximus. Nulla facilisi. Donec placerat porttitor accumsan. Aliquam cursus ultrices laoreet. Vivamus in pulvinar orci. Pellentesque tempus ex quis mi scelerisque, in interdum libero convallis. Ut feugiat enim neque, ut imperdiet elit consectetur id. Vivamus non magna non purus maximus euismod. Cras mollis magna et tincidunt posuere. In hac habitasse platea dictumst. Suspendisse potenti. Aliquam at enim at eros dapibus consequat vitae non purus.</p>
<p>Nullam dapibus egestas sagittis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean id auctor metus, vel ultricies risus. Quisque id semper tortor, id mollis tellus. Quisque interdum, diam in hendrerit mollis, velit dolor tristique ipsum, ac condimentum nunc enim quis nulla. Fusce dictum est vitae nunc pharetra efficitur. Morbi et tellus dignissim, tempor nisi a, efficitur purus. Nam et velit ac nisi semper finibus vitae sed tortor. Mauris luctus enim in sapien egestas dapibus. Pellentesque velit justo, gravida ut ultricies feugiat, vulputate at lectus. Vivamus est leo, suscipit eget lectus pellentesque, euismod lobortis dolor. Maecenas pretium efficitur tellus.</p>
<p>Donec eget velit tellus. Nam fermentum sem nibh, et volutpat tortor dictum sit amet. Proin id viverra eros. Mauris ullamcorper ullamcorper magna non auctor. Duis eget sem bibendum, lacinia quam sed, gravida massa. Vestibulum viverra sed tortor sed suscipit. Etiam tempor purus augue, sed egestas tellus gravida id. Pellentesque egestas massa interdum tincidunt blandit. Curabitur vehicula purus in erat malesuada sodales. Sed ut fringilla lectus. Ut non consequat tellus. Donec dictum libero quis ipsum condimentum dapibus. Curabitur accumsan nisl id est placerat, posuere ornare est consectetur. Maecenas hendrerit, massa in mollis consequat, lacus felis aliquam sem, non pharetra est libero in nibh. Etiam et dapibus neque, finibus iaculis nibh.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_location' => array(),
  'field_skills' => array(
    'und' => array(
      0 => array(
        'tid' => 48,
        'uuid' => '0eb68aec-f893-4a75-96e5-d0785e6c2243',
      ),
      1 => array(
        'tid' => 53,
        'uuid' => 'c5b79deb-ed27-40ad-a71e-446a3f4f8291',
      ),
      2 => array(
        'tid' => 61,
        'uuid' => '93db89b4-72a5-428e-8b6f-fba654473e88',
      ),
      3 => array(
        'tid' => 60,
        'uuid' => 'd8862d10-097f-44e7-a03f-d55df117f845',
      ),
    ),
  ),
  'field_organization' => array(
    'und' => array(
      0 => array(
        'value' => 'FastPlatypus',
        'format' => NULL,
        'safe_value' => 'FastPlatypus',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 0,
  'comment_count' => 0,
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'pathauto_perform_alias' => FALSE,
  'date' => '2016-02-15 11:50:35 +0100',
);
  $nodes[] = array(
  'uid' => 0,
  'title' => 'Communication manager',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'vuuid' => 'ae9dfc1b-1718-4068-9af8-49c664c509df',
  'type' => 'experience',
  'language' => 'fr',
  'created' => 1455533742,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '8b2eb762-29de-471f-9b57-b0d1eb6fc3b5',
  'revision_uid' => 1,
  'field_date' => array(
    'und' => array(
      0 => array(
        'value' => '2004-01-01 00:00:00',
        'value2' => '2010-01-01 00:00:00',
        'timezone' => 'Europe/Paris',
        'timezone_db' => 'Europe/Paris',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_image' => array(),
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pulvinar malesuada pellentesque. Proin vestibulum commodo imperdiet. Sed aliquet sed erat at ultrices. Morbi nisi nibh, aliquet eu leo in, tempus rhoncus sapien. Nunc interdum nulla lorem, nec fermentum massa pulvinar at. Ut vestibulum enim sit amet justo vehicula scelerisque. Nulla arcu ante, porttitor quis interdum et, ultrices ut odio. Nulla faucibus pellentesque vulputate. Nulla viverra ornare magna, id fermentum nulla accumsan vitae. In eu vehicula elit, nec iaculis nulla. Quisque quis turpis nunc. Donec fringilla euismod mauris. Nullam in metus ac turpis ornare faucibus. Suspendisse vitae nisl risus. Suspendisse in purus risus.</p>

<p>Aenean varius fermentum elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean commodo rutrum egestas. Pellentesque in lorem pulvinar, convallis velit id, dapibus risus. Suspendisse in pharetra felis. Vivamus bibendum augue quis mauris egestas blandit. Vestibulum auctor at est sagittis lacinia. Vestibulum euismod, tellus sed commodo vestibulum, erat arcu varius ligula, at mollis sapien lacus at augue. Morbi risus nisi, gravida ac aliquam a, pretium quis sem.</p>

<p>Sed viverra eu urna luctus maximus. Ut nec semper dolor. Praesent imperdiet id elit a placerat. Morbi ac velit at tortor consequat molestie. Morbi urna sem, malesuada et varius vel, ultricies quis massa. Phasellus mi lorem, blandit porttitor magna ut, bibendum tristique odio. Aliquam erat volutpat. Curabitur aliquet eleifend laoreet. Suspendisse nisi mi, hendrerit a ultricies in, interdum eget quam.</p>

<p>Aliquam erat volutpat. Fusce lobortis arcu et orci maximus malesuada. Duis lectus erat, finibus eu bibendum id, mollis eget neque. In ipsum orci, vestibulum ac tellus vitae, ultricies commodo libero. Donec sagittis nibh in nulla rhoncus, vel tempus ligula faucibus. Morbi in ipsum eu purus placerat iaculis. Nunc vulputate volutpat tincidunt. Mauris nec leo ex. Mauris viverra dictum eros nec pharetra. Donec vitae luctus turpis. Vestibulum facilisis, leo non porta vehicula, nulla ex sagittis sem, eu lobortis velit sapien et neque. Nulla molestie eget risus vel mollis. Donec arcu erat, commodo in semper eleifend, scelerisque et justo. Etiam in ipsum est. Etiam quis aliquam arcu, at imperdiet dolor.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pulvinar malesuada pellentesque. Proin vestibulum commodo imperdiet. Sed aliquet sed erat at ultrices. Morbi nisi nibh, aliquet eu leo in, tempus rhoncus sapien. Nunc interdum nulla lorem, nec fermentum massa pulvinar at. Ut vestibulum enim sit amet justo vehicula scelerisque. Nulla arcu ante, porttitor quis interdum et, ultrices ut odio. Nulla faucibus pellentesque vulputate. Nulla viverra ornare magna, id fermentum nulla accumsan vitae. In eu vehicula elit, nec iaculis nulla. Quisque quis turpis nunc. Donec fringilla euismod mauris. Nullam in metus ac turpis ornare faucibus. Suspendisse vitae nisl risus. Suspendisse in purus risus.</p>
<p>Aenean varius fermentum elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean commodo rutrum egestas. Pellentesque in lorem pulvinar, convallis velit id, dapibus risus. Suspendisse in pharetra felis. Vivamus bibendum augue quis mauris egestas blandit. Vestibulum auctor at est sagittis lacinia. Vestibulum euismod, tellus sed commodo vestibulum, erat arcu varius ligula, at mollis sapien lacus at augue. Morbi risus nisi, gravida ac aliquam a, pretium quis sem.</p>
<p>Sed viverra eu urna luctus maximus. Ut nec semper dolor. Praesent imperdiet id elit a placerat. Morbi ac velit at tortor consequat molestie. Morbi urna sem, malesuada et varius vel, ultricies quis massa. Phasellus mi lorem, blandit porttitor magna ut, bibendum tristique odio. Aliquam erat volutpat. Curabitur aliquet eleifend laoreet. Suspendisse nisi mi, hendrerit a ultricies in, interdum eget quam.</p>
<p>Aliquam erat volutpat. Fusce lobortis arcu et orci maximus malesuada. Duis lectus erat, finibus eu bibendum id, mollis eget neque. In ipsum orci, vestibulum ac tellus vitae, ultricies commodo libero. Donec sagittis nibh in nulla rhoncus, vel tempus ligula faucibus. Morbi in ipsum eu purus placerat iaculis. Nunc vulputate volutpat tincidunt. Mauris nec leo ex. Mauris viverra dictum eros nec pharetra. Donec vitae luctus turpis. Vestibulum facilisis, leo non porta vehicula, nulla ex sagittis sem, eu lobortis velit sapien et neque. Nulla molestie eget risus vel mollis. Donec arcu erat, commodo in semper eleifend, scelerisque et justo. Etiam in ipsum est. Etiam quis aliquam arcu, at imperdiet dolor.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_location' => array(),
  'field_skills' => array(
    'und' => array(
      0 => array(
        'tid' => 61,
        'uuid' => '93db89b4-72a5-428e-8b6f-fba654473e88',
      ),
      1 => array(
        'tid' => 60,
        'uuid' => 'd8862d10-097f-44e7-a03f-d55df117f845',
      ),
      2 => array(
        'tid' => 58,
        'uuid' => 'dd5035ec-7e92-4a30-9e7f-8d9fb8321f32',
      ),
      3 => array(
        'tid' => 53,
        'uuid' => 'c5b79deb-ed27-40ad-a71e-446a3f4f8291',
      ),
      4 => array(
        'tid' => 70,
        'uuid' => '50077e43-b14c-4912-b7fa-a5b2fd278ac3',
      ),
      5 => array(
        'tid' => 48,
        'uuid' => '0eb68aec-f893-4a75-96e5-d0785e6c2243',
      ),
      6 => array(
        'tid' => 50,
        'uuid' => '27d6017b-ea15-4efa-a35d-0e783143bbdb',
      ),
    ),
  ),
  'field_organization' => array(
    'und' => array(
      0 => array(
        'value' => 'FastPlatypus',
        'format' => NULL,
        'safe_value' => 'FastPlatypus',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 0,
  'comment_count' => 0,
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'pathauto_perform_alias' => FALSE,
  'date' => '2016-02-15 11:55:42 +0100',
);
  $nodes[] = array(
  'uid' => 0,
  'title' => 'Senior Consultant',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'vuuid' => '28cb9f81-df60-42a8-8841-30dbf06f6e17',
  'type' => 'experience',
  'language' => 'fr',
  'created' => 1455534356,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'ea2a2f92-dcd2-4596-a8d4-3936966d8937',
  'revision_uid' => 1,
  'field_date' => array(
    'und' => array(
      0 => array(
        'value' => '2010-01-01 00:00:00',
        'value2' => '2016-01-01 00:00:00',
        'timezone' => 'Europe/Paris',
        'timezone_db' => 'Europe/Paris',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_image' => array(),
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus non dui vulputate, malesuada purus nec, porttitor augue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam sit amet sapien sed sapien porttitor venenatis. Morbi volutpat enim sed magna finibus fringilla. Aliquam ac arcu vitae velit tristique maximus. Nam vel porttitor ligula, nec varius enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In hac habitasse platea dictumst.</p>

<p>Fusce mattis tellus est, nec tincidunt est suscipit nec. Sed pharetra suscipit ex vel pulvinar. Suspendisse tempus tellus ipsum, quis scelerisque libero tempor non. Duis convallis dignissim nisl, fringilla eleifend lectus sagittis vel. Vestibulum porta neque et mi gravida, et tincidunt mauris ornare. Nulla rhoncus libero ante, vel consequat mauris varius vitae. Aliquam erat volutpat. Duis ut eros suscipit, rutrum neque nec, varius dui. Nulla et elit augue. Integer justo mauris, commodo nec tempus vel, volutpat at ante.</p>

<p>Proin eget mi ac felis hendrerit mattis. Morbi lacinia hendrerit dui, in consequat orci ultricies sit amet. Maecenas euismod, nunc a aliquet volutpat, odio eros egestas nibh, egestas consectetur nibh ipsum non eros. Aliquam nisl sapien, pellentesque quis elementum sed, vestibulum nec risus. Curabitur efficitur vitae mi vitae ornare. Praesent at laoreet libero. Nullam finibus justo ac nunc ultricies pretium. Nunc id erat volutpat, tristique sem sit amet, rhoncus nisi. Quisque in orci a nisl sollicitudin euismod. Nam sed odio volutpat, faucibus erat vitae, semper massa. Cras non venenatis turpis. Quisque at tortor laoreet libero mattis semper egestas egestas ex.</p>

<p>Duis varius magna et tortor porttitor pharetra. Nam a dui orci. Integer mi velit, convallis commodo ante in, suscipit varius nisl. Donec aliquet metus ac odio faucibus, consectetur tempus lorem facilisis. Quisque vestibulum, elit ut tempor sollicitudin, nibh mauris molestie lacus, non tempus nunc purus in elit. Sed ligula quam, scelerisque vel massa a, sodales semper mi. Aliquam sit amet neque pretium mi tristique porttitor. Proin ac dui quam. Ut commodo dui eleifend nunc pretium, nec pulvinar libero hendrerit. Praesent tristique in lacus id volutpat. Nam bibendum nisi vel dui tincidunt, id vehicula eros lobortis. Donec arcu est, porta sed bibendum sit amet, commodo vel elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eu aliquet turpis. Maecenas maximus iaculis accumsan.</p>

<p>Nunc lobortis neque non turpis eleifend, eu elementum magna tristique. Nulla et mollis elit, ut pulvinar odio. Donec a nibh lorem. Suspendisse dictum fringilla nisl, quis ultricies velit finibus ac. Nullam nec pharetra erat. Phasellus pretium sit amet mi eu elementum. Pellentesque sit amet luctus libero. Nunc turpis enim, tristique nec cursus id, mattis ut est. Phasellus hendrerit eleifend nisi, vel placerat leo suscipit ut. Phasellus arcu elit, dapibus in suscipit ut, consequat eu dui. Donec pretium maximus purus, in viverra ante feugiat imperdiet.</p>
</div>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus non dui vulputate, malesuada purus nec, porttitor augue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam sit amet sapien sed sapien porttitor venenatis. Morbi volutpat enim sed magna finibus fringilla. Aliquam ac arcu vitae velit tristique maximus. Nam vel porttitor ligula, nec varius enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In hac habitasse platea dictumst.</p>
<p>Fusce mattis tellus est, nec tincidunt est suscipit nec. Sed pharetra suscipit ex vel pulvinar. Suspendisse tempus tellus ipsum, quis scelerisque libero tempor non. Duis convallis dignissim nisl, fringilla eleifend lectus sagittis vel. Vestibulum porta neque et mi gravida, et tincidunt mauris ornare. Nulla rhoncus libero ante, vel consequat mauris varius vitae. Aliquam erat volutpat. Duis ut eros suscipit, rutrum neque nec, varius dui. Nulla et elit augue. Integer justo mauris, commodo nec tempus vel, volutpat at ante.</p>
<p>Proin eget mi ac felis hendrerit mattis. Morbi lacinia hendrerit dui, in consequat orci ultricies sit amet. Maecenas euismod, nunc a aliquet volutpat, odio eros egestas nibh, egestas consectetur nibh ipsum non eros. Aliquam nisl sapien, pellentesque quis elementum sed, vestibulum nec risus. Curabitur efficitur vitae mi vitae ornare. Praesent at laoreet libero. Nullam finibus justo ac nunc ultricies pretium. Nunc id erat volutpat, tristique sem sit amet, rhoncus nisi. Quisque in orci a nisl sollicitudin euismod. Nam sed odio volutpat, faucibus erat vitae, semper massa. Cras non venenatis turpis. Quisque at tortor laoreet libero mattis semper egestas egestas ex.</p>
<p>Duis varius magna et tortor porttitor pharetra. Nam a dui orci. Integer mi velit, convallis commodo ante in, suscipit varius nisl. Donec aliquet metus ac odio faucibus, consectetur tempus lorem facilisis. Quisque vestibulum, elit ut tempor sollicitudin, nibh mauris molestie lacus, non tempus nunc purus in elit. Sed ligula quam, scelerisque vel massa a, sodales semper mi. Aliquam sit amet neque pretium mi tristique porttitor. Proin ac dui quam. Ut commodo dui eleifend nunc pretium, nec pulvinar libero hendrerit. Praesent tristique in lacus id volutpat. Nam bibendum nisi vel dui tincidunt, id vehicula eros lobortis. Donec arcu est, porta sed bibendum sit amet, commodo vel elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eu aliquet turpis. Maecenas maximus iaculis accumsan.</p>
<p>Nunc lobortis neque non turpis eleifend, eu elementum magna tristique. Nulla et mollis elit, ut pulvinar odio. Donec a nibh lorem. Suspendisse dictum fringilla nisl, quis ultricies velit finibus ac. Nullam nec pharetra erat. Phasellus pretium sit amet mi eu elementum. Pellentesque sit amet luctus libero. Nunc turpis enim, tristique nec cursus id, mattis ut est. Phasellus hendrerit eleifend nisi, vel placerat leo suscipit ut. Phasellus arcu elit, dapibus in suscipit ut, consequat eu dui. Donec pretium maximus purus, in viverra ante feugiat imperdiet.</p>
</div>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_location' => array(),
  'field_skills' => array(
    'und' => array(
      0 => array(
        'tid' => 61,
        'uuid' => '93db89b4-72a5-428e-8b6f-fba654473e88',
      ),
      1 => array(
        'tid' => 68,
        'uuid' => 'f2dba017-833b-4b20-8ab0-49fc0802a95a',
      ),
      2 => array(
        'tid' => 69,
        'uuid' => '48095ee8-bb55-42e4-9407-cf8d8d8ccb76',
      ),
      3 => array(
        'tid' => 56,
        'uuid' => '2d19f9ad-8bf2-41d1-b119-762fa7e01f03',
      ),
      4 => array(
        'tid' => 50,
        'uuid' => '27d6017b-ea15-4efa-a35d-0e783143bbdb',
      ),
      5 => array(
        'tid' => 60,
        'uuid' => 'd8862d10-097f-44e7-a03f-d55df117f845',
      ),
      6 => array(
        'tid' => 58,
        'uuid' => 'dd5035ec-7e92-4a30-9e7f-8d9fb8321f32',
      ),
    ),
  ),
  'field_organization' => array(
    'und' => array(
      0 => array(
        'value' => 'Ofle Communication ',
        'format' => NULL,
        'safe_value' => 'Ofle Communication ',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 0,
  'comment_count' => 0,
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'pathauto_perform_alias' => FALSE,
  'date' => '2016-02-15 12:05:56 +0100',
);
  return $nodes;
}
