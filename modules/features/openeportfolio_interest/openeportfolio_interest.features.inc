<?php
/**
 * @file
 * openeportfolio_interest.features.inc
 */

/**
 * Implements hook_views_api().
 */
function openeportfolio_interest_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function openeportfolio_interest_node_info() {
  $items = array(
    'interest' => array(
      'name' => t('Interest'),
      'base' => 'node_content',
      'description' => t('Ajoutez vos centres d’intérêts personnels ou professionnels (<i>Exemple : Sport, Musique, Association, Film, Bénévolat ...</i>).'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
