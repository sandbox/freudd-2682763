<?php
/**
 * @file
 * openeportfolio_experience.features.inc
 */

/**
 * Implements hook_views_api().
 */
function openeportfolio_experience_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function openeportfolio_experience_node_info() {
  $items = array(
    'experience' => array(
      'name' => t('Experience'),
      'base' => 'node_content',
      'description' => t('Ajoutez vos expériences professionnelles (<i>Exemple : Stage, Travail saisonnier, CDD, CDI ...</i>)'),
      'has_title' => '1',
      'title_label' => t('Poste'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
