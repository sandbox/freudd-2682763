<?php
/**
 * @file
 * openeportfolio_education_demo.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function openeportfolio_education_demo_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 0,
  'title' => '2 years technical degree in communication',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'vuuid' => 'a565eb57-f89e-4a12-b432-ea68a95514b9',
  'type' => 'education',
  'language' => 'fr',
  'created' => 1455536026,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'bc9652b4-5fae-49e1-b195-f491f52e74bc',
  'revision_uid' => 1,
  'field_image' => array(),
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac fringilla turpis. Sed at laoreet ex, quis placerat diam. Maecenas et molestie tortor. Maecenas interdum diam augue, non pretium leo egestas ac. Integer ultricies facilisis tortor, at consectetur nisi convallis vel. Nunc in lectus ut nibh sollicitudin ullamcorper. Sed molestie sem sit amet nibh semper rutrum.</p>

<p>Aenean varius finibus dolor, volutpat malesuada tortor porta eu. Vivamus nec sodales nisl. Nam facilisis vitae erat a malesuada. Curabitur lacinia in elit in tincidunt. Praesent dictum, nisi vel tempor ultrices, felis purus condimentum magna, et tincidunt tellus mi sit amet odio. Maecenas et viverra mauris. Maecenas eget ultrices massa. Fusce tempus mauris id lacinia efficitur. Nullam rhoncus accumsan ipsum, quis convallis odio commodo a. Curabitur at urna in nisi faucibus volutpat eu eu ante. Integer ultrices condimentum vestibulum. Ut id est quis augue pulvinar sagittis sed eget magna. Cras aliquam lorem vitae lacus lobortis aliquam. Nunc sodales tincidunt turpis, et rutrum mauris.</p>

<p>Maecenas in lacus nec leo posuere ultrices. Nullam diam nisl, aliquam eget tristique ac, iaculis at ipsum. Phasellus suscipit venenatis pellentesque. Donec ante tortor, elementum a viverra a, condimentum eu odio. Mauris ligula augue, condimentum eu imperdiet fermentum, semper blandit quam. Maecenas malesuada tortor ex, id dignissim ligula feugiat id. Proin ut mauris vitae arcu hendrerit pharetra. Cras cursus facilisis tellus, eu viverra metus dignissim id.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac fringilla turpis. Sed at laoreet ex, quis placerat diam. Maecenas et molestie tortor. Maecenas interdum diam augue, non pretium leo egestas ac. Integer ultricies facilisis tortor, at consectetur nisi convallis vel. Nunc in lectus ut nibh sollicitudin ullamcorper. Sed molestie sem sit amet nibh semper rutrum.</p>
<p>Aenean varius finibus dolor, volutpat malesuada tortor porta eu. Vivamus nec sodales nisl. Nam facilisis vitae erat a malesuada. Curabitur lacinia in elit in tincidunt. Praesent dictum, nisi vel tempor ultrices, felis purus condimentum magna, et tincidunt tellus mi sit amet odio. Maecenas et viverra mauris. Maecenas eget ultrices massa. Fusce tempus mauris id lacinia efficitur. Nullam rhoncus accumsan ipsum, quis convallis odio commodo a. Curabitur at urna in nisi faucibus volutpat eu eu ante. Integer ultrices condimentum vestibulum. Ut id est quis augue pulvinar sagittis sed eget magna. Cras aliquam lorem vitae lacus lobortis aliquam. Nunc sodales tincidunt turpis, et rutrum mauris.</p>
<p>Maecenas in lacus nec leo posuere ultrices. Nullam diam nisl, aliquam eget tristique ac, iaculis at ipsum. Phasellus suscipit venenatis pellentesque. Donec ante tortor, elementum a viverra a, condimentum eu odio. Mauris ligula augue, condimentum eu imperdiet fermentum, semper blandit quam. Maecenas malesuada tortor ex, id dignissim ligula feugiat id. Proin ut mauris vitae arcu hendrerit pharetra. Cras cursus facilisis tellus, eu viverra metus dignissim id.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_date' => array(
    'und' => array(
      0 => array(
        'value' => '1996-01-01 00:00:00',
        'value2' => '1998-01-01 00:00:00',
        'timezone' => 'Europe/Paris',
        'timezone_db' => 'Europe/Paris',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_location' => array(),
  'field_skills' => array(
    'und' => array(
      0 => array(
        'tid' => 61,
        'uuid' => '93db89b4-72a5-428e-8b6f-fba654473e88',
      ),
      1 => array(
        'tid' => 50,
        'uuid' => '27d6017b-ea15-4efa-a35d-0e783143bbdb',
      ),
      2 => array(
        'tid' => 49,
        'uuid' => 'e0c9fa6c-9a1f-439f-99f0-74f9dab0c758',
      ),
    ),
  ),
  'field_organization' => array(
    'und' => array(
      0 => array(
        'value' => 'Université de Montréal',
        'format' => 'filtered_html',
        'safe_value' => 'Université de Montréal',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 0,
  'comment_count' => 0,
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'pathauto_perform_alias' => FALSE,
  'date' => '2016-02-15 12:33:46 +0100',
);
  $nodes[] = array(
  'uid' => 0,
  'title' => 'Fifth year of university studies in management',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'vuuid' => '74fcde3e-3701-4277-8c19-aab7852ebc53',
  'type' => 'education',
  'language' => 'fr',
  'created' => 1455539485,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'c4b263b9-7a27-490a-9848-418773252f0b',
  'revision_uid' => 1,
  'field_image' => array(),
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sodales blandit est, non pellentesque ipsum lobortis eget. Sed porttitor finibus scelerisque. Duis tempus volutpat nunc, in vulputate sem luctus et. Nam eleifend mi non ultricies maximus. Phasellus tempor ipsum ligula, at efficitur sem euismod in. Phasellus venenatis viverra mi, sed ultricies turpis sollicitudin interdum. Nullam scelerisque purus eu elit dictum iaculis. Etiam arcu lectus, maximus non facilisis ut, vulputate nec odio. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>

<p>Morbi faucibus enim risus, non eleifend tellus egestas a. Proin vel tellus vel leo faucibus bibendum. Duis pulvinar commodo justo, a sodales justo semper quis. Proin lorem sem, vestibulum eu vulputate id, venenatis sed enim. Proin in iaculis massa. Integer convallis a metus ut ultricies. Donec fermentum ex vel diam sagittis mattis. Vestibulum sagittis ac tellus porta ultrices. Aliquam molestie blandit mi non tempus. Curabitur nec malesuada ex. Donec vehicula cursus est in sodales. Nunc in mi eget diam ultricies malesuada non quis justo. Duis ornare tristique lectus ac bibendum. Nam lobortis fringilla arcu, maximus bibendum urna maximus non. Aenean lobortis neque vitae ex tempus, eget consectetur sapien sagittis. Nullam pharetra libero leo, a fermentum nulla porta non.</p>

<p>Proin faucibus et leo ut porta. Nullam vestibulum velit nisi, sodales elementum libero blandit vestibulum. Nam porttitor nec sapien et condimentum. Aenean in suscipit augue, vel ultricies quam. Proin at ultricies leo. Nulla id accumsan diam. Maecenas eleifend diam dolor, ac imperdiet dolor vestibulum ut. Pellentesque euismod congue lacus, id maximus diam laoreet in.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sodales blandit est, non pellentesque ipsum lobortis eget. Sed porttitor finibus scelerisque. Duis tempus volutpat nunc, in vulputate sem luctus et. Nam eleifend mi non ultricies maximus. Phasellus tempor ipsum ligula, at efficitur sem euismod in. Phasellus venenatis viverra mi, sed ultricies turpis sollicitudin interdum. Nullam scelerisque purus eu elit dictum iaculis. Etiam arcu lectus, maximus non facilisis ut, vulputate nec odio. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
<p>Morbi faucibus enim risus, non eleifend tellus egestas a. Proin vel tellus vel leo faucibus bibendum. Duis pulvinar commodo justo, a sodales justo semper quis. Proin lorem sem, vestibulum eu vulputate id, venenatis sed enim. Proin in iaculis massa. Integer convallis a metus ut ultricies. Donec fermentum ex vel diam sagittis mattis. Vestibulum sagittis ac tellus porta ultrices. Aliquam molestie blandit mi non tempus. Curabitur nec malesuada ex. Donec vehicula cursus est in sodales. Nunc in mi eget diam ultricies malesuada non quis justo. Duis ornare tristique lectus ac bibendum. Nam lobortis fringilla arcu, maximus bibendum urna maximus non. Aenean lobortis neque vitae ex tempus, eget consectetur sapien sagittis. Nullam pharetra libero leo, a fermentum nulla porta non.</p>
<p>Proin faucibus et leo ut porta. Nullam vestibulum velit nisi, sodales elementum libero blandit vestibulum. Nam porttitor nec sapien et condimentum. Aenean in suscipit augue, vel ultricies quam. Proin at ultricies leo. Nulla id accumsan diam. Maecenas eleifend diam dolor, ac imperdiet dolor vestibulum ut. Pellentesque euismod congue lacus, id maximus diam laoreet in.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_date' => array(
    'und' => array(
      0 => array(
        'value' => '1999-01-01 00:00:00',
        'value2' => '2001-01-01 00:00:00',
        'timezone' => 'Europe/Paris',
        'timezone_db' => 'Europe/Paris',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_location' => array(
    'und' => array(
      0 => array(
        'value' => 'Canada',
        'format' => NULL,
        'safe_value' => 'Canada',
      ),
    ),
  ),
  'field_skills' => array(
    'und' => array(
      0 => array(
        'tid' => 48,
        'uuid' => '0eb68aec-f893-4a75-96e5-d0785e6c2243',
      ),
      1 => array(
        'tid' => 50,
        'uuid' => '27d6017b-ea15-4efa-a35d-0e783143bbdb',
      ),
      2 => array(
        'tid' => 68,
        'uuid' => 'f2dba017-833b-4b20-8ab0-49fc0802a95a',
      ),
      3 => array(
        'tid' => 69,
        'uuid' => '48095ee8-bb55-42e4-9407-cf8d8d8ccb76',
      ),
      4 => array(
        'tid' => 56,
        'uuid' => '2d19f9ad-8bf2-41d1-b119-762fa7e01f03',
      ),
      5 => array(
        'tid' => 60,
        'uuid' => 'd8862d10-097f-44e7-a03f-d55df117f845',
      ),
    ),
  ),
  'field_organization' => array(
    'und' => array(
      0 => array(
        'value' => 'Université de Montréal',
        'format' => NULL,
        'safe_value' => 'Université de Montréal',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 0,
  'comment_count' => 0,
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'pathauto_perform_alias' => FALSE,
  'date' => '2016-02-15 13:31:25 +0100',
);
  $nodes[] = array(
  'uid' => 0,
  'title' => '3 years university degree in communication',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 1,
  'sticky' => 0,
  'vuuid' => '75f65686-7c52-4e54-b337-d5b15e924f7a',
  'type' => 'education',
  'language' => 'fr',
  'created' => 1455536879,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'f78b2718-edf8-458b-b3e0-e12dab57d70f',
  'revision_uid' => 1,
  'field_image' => array(),
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id elit blandit, lacinia orci in, eleifend nisi. Cras feugiat sollicitudin nisi. Proin et condimentum dui. Sed sit amet dui odio. Sed vel tempor leo, tempus mattis lacus. Sed mollis tortor ex, eget feugiat libero commodo nec. Praesent fringilla hendrerit pellentesque. Curabitur sit amet molestie est. Pellentesque eu bibendum tortor. Pellentesque euismod viverra accumsan. Nunc id dui nisi. Donec a leo molestie, consectetur ipsum sit amet, rhoncus felis.</p>

<p>Proin sed viverra justo. Suspendisse et aliquam mi. Curabitur sit amet nibh tortor. Nunc ultrices eu sem vitae rhoncus. In nec est ac lorem blandit malesuada at eget ipsum. Donec iaculis pretium justo, vel consequat risus hendrerit sed. Nunc varius tempor diam, a vehicula leo tincidunt eget.</p>

<p>Nam commodo, turpis eu vehicula porttitor, nisi ligula interdum tellus, quis viverra massa dolor at purus. Aliquam ac nisi eros. Vivamus ultricies rhoncus ipsum quis blandit. Sed consequat id orci eu ultrices. Vestibulum accumsan augue sit amet cursus luctus. Cras placerat elementum ligula, et ultrices risus blandit posuere. Fusce convallis non lectus eu bibendum. Praesent eu sodales augue. Mauris elit mi, interdum euismod elementum sit amet, sagittis eu erat. Sed lobortis, nulla nec fringilla auctor, metus est elementum metus, ut suscipit massa diam in urna. Ut commodo ipsum enim, vel elementum quam convallis non. Nam id ultricies tortor, eu tincidunt arcu. Cras eu tempus enim. Suspendisse potenti. Vivamus ultrices sapien malesuada feugiat consectetur.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id elit blandit, lacinia orci in, eleifend nisi. Cras feugiat sollicitudin nisi. Proin et condimentum dui. Sed sit amet dui odio. Sed vel tempor leo, tempus mattis lacus. Sed mollis tortor ex, eget feugiat libero commodo nec. Praesent fringilla hendrerit pellentesque. Curabitur sit amet molestie est. Pellentesque eu bibendum tortor. Pellentesque euismod viverra accumsan. Nunc id dui nisi. Donec a leo molestie, consectetur ipsum sit amet, rhoncus felis.</p>
<p>Proin sed viverra justo. Suspendisse et aliquam mi. Curabitur sit amet nibh tortor. Nunc ultrices eu sem vitae rhoncus. In nec est ac lorem blandit malesuada at eget ipsum. Donec iaculis pretium justo, vel consequat risus hendrerit sed. Nunc varius tempor diam, a vehicula leo tincidunt eget.</p>
<p>Nam commodo, turpis eu vehicula porttitor, nisi ligula interdum tellus, quis viverra massa dolor at purus. Aliquam ac nisi eros. Vivamus ultricies rhoncus ipsum quis blandit. Sed consequat id orci eu ultrices. Vestibulum accumsan augue sit amet cursus luctus. Cras placerat elementum ligula, et ultrices risus blandit posuere. Fusce convallis non lectus eu bibendum. Praesent eu sodales augue. Mauris elit mi, interdum euismod elementum sit amet, sagittis eu erat. Sed lobortis, nulla nec fringilla auctor, metus est elementum metus, ut suscipit massa diam in urna. Ut commodo ipsum enim, vel elementum quam convallis non. Nam id ultricies tortor, eu tincidunt arcu. Cras eu tempus enim. Suspendisse potenti. Vivamus ultrices sapien malesuada feugiat consectetur.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_date' => array(
    'und' => array(
      0 => array(
        'value' => '1998-01-01 00:00:00',
        'value2' => '1999-01-01 00:00:00',
        'timezone' => 'Europe/Paris',
        'timezone_db' => 'Europe/Paris',
        'date_type' => 'datetime',
      ),
    ),
  ),
  'field_location' => array(
    'und' => array(
      0 => array(
        'value' => 'Canada',
        'format' => NULL,
        'safe_value' => 'Canada',
      ),
    ),
  ),
  'field_skills' => array(
    'und' => array(
      0 => array(
        'tid' => 63,
        'uuid' => '58fd64a0-b994-481b-860d-b2bc8c3dc8aa',
      ),
      1 => array(
        'tid' => 49,
        'uuid' => 'e0c9fa6c-9a1f-439f-99f0-74f9dab0c758',
      ),
      2 => array(
        'tid' => 60,
        'uuid' => 'd8862d10-097f-44e7-a03f-d55df117f845',
      ),
    ),
  ),
  'field_organization' => array(
    'und' => array(
      0 => array(
        'value' => 'Université de Montréal',
        'format' => NULL,
        'safe_value' => 'Université de Montréal',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 0,
  'comment_count' => 0,
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'pathauto_perform_alias' => FALSE,
  'date' => '2016-02-15 12:47:59 +0100',
);
  return $nodes;
}
