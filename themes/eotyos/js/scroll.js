/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */
 
 function strstr(haystack, needle, bool) {
	var pos = 0;

    haystack += "";
    pos = haystack.indexOf(needle); if (pos == -1) {
        return false;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
}

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

	// To understand behaviors, see https://drupal.org/node/756722#behaviors
	Drupal.behaviors.my_custom_behavior = {
	  attach: function(context, settings) {

		// Place your code here.
		$('.'+settings.class_scroll).click( function() { // Au clic sur un �l�ment
			var link = $(this).attr('href'); // Page cible
			var page = strstr(link, '#', false);
			var speed = parseInt(settings.speed_scroll); // Dur�e de l'animation (en ms)
			$('html, body').animate( { scrollTop: $(page).offset().top -80 }, speed ); // Go
			return false;
		});
	  }
	};

})(jQuery, Drupal, this, this.document);