; openeportfolio make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[ctools][version] = "1.9"
projects[ctools][subdir] = "contrib"

projects[date][version] = "2.9"
projects[date][subdir] = "contrib"

projects[features][version] = "2.7"
projects[features][subdir] = "contrib"

projects[uuid_features][version] = "1.0-alpha4"
projects[uuid_features][subdir] = "contrib"

projects[link][version] = "1.4"
projects[link][subdir] = "contrib"

projects[imce][version] = "1.9"
projects[imce][subdir] = "contrib"

projects[l10n_update][version] = "1.1"
projects[l10n_update][subdir] = "contrib"

;projects[openeportfolio_basic_page][version] = "1.0"
;projects[openeportfolio_basic_page][subdir] = "features"

projects[openeportfolio_blog][version] = "1.0"
projects[openeportfolio_blog][subdir] = "features"

projects[openeportfolio_captcha][version] = "1.0"
projects[openeportfolio_captcha][subdir] = "features"

projects[openeportfolio_education][version] = "1.0"
projects[openeportfolio_education][subdir] = "features"

projects[openeportfolio_experience][version] = "1.0"
projects[openeportfolio_experience][subdir] = "features"

projects[openeportfolio_field][version] = "1.0"
projects[openeportfolio_field][subdir] = "features"

projects[openeportfolio_filters][version] = "1.0"
projects[openeportfolio_filters][subdir] = "features"

projects[openeportfolio_home_page][version] = "1.0"
projects[openeportfolio_home_page][subdir] = "features"

projects[openeportfolio_image_styles][version] = "1.0"
projects[openeportfolio_image_styles][subdir] = "features"

projects[openeportfolio_interest][version] = "1.0"
projects[openeportfolio_interest][subdir] = "features"

projects[openeportfolio_menu][version] = "1.0"
projects[openeportfolio_menu][subdir] = "features"

projects[openeportfolio_project][version] = "1.0"
projects[openeportfolio_project][subdir] = "features"

projects[openeportfolio_roles_permissions][version] = "1.0"
projects[openeportfolio_roles_permissions][subdir] = "features"

projects[openeportfolio_skills][version] = "1.0"
projects[openeportfolio_skills][subdir] = "features"

projects[openeportfolio_tags_demo][version] = "1.0"
projects[openeportfolio_tags_demo][subdir] = "demo"

projects[openeportfolio_skills_demo][version] = "1.0"
projects[openeportfolio_skills_demo][subdir] = "demo"

projects[openeportfolio_blog_demo][version] = "1.0"
projects[openeportfolio_blog_demo][subdir] = "demo"

projects[openeportfolio_education_demo][version] = "1.0"
projects[openeportfolio_education_demo][subdir] = "demo"

projects[openeportfolio_experience_demo][version] = "1.0"
projects[openeportfolio_experience_demo][subdir] = "demo"

projects[openeportfolio_interest_demo][version] = "1.0"
projects[openeportfolio_interest_demo][subdir] = "demo"

projects[openeportfolio_project_demo][version] = "1.0"
projects[openeportfolio_project_demo][subdir] = "demo"

projects[entity][version] = "1.6"
projects[entity][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[menu_attributes][version] = "1.0"
projects[menu_attributes][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[transliteration][version] = "3.2"
projects[transliteration][subdir] = "contrib"

projects[panels][version] = "3.5"
projects[panels][subdir] = "contrib"

projects[globalredirect][version] = "1.5"
projects[globalredirect][subdir] = "contrib"

projects[metatag][version] = "1.13"
projects[metatag][subdir] = "contrib"

projects[captcha][version] = "1.3"
projects[captcha][subdir] = "contrib"

projects[uuid][version] = "1.0-beta1"
projects[uuid][subdir] = "contrib"

projects[ckeditor][version] = "1.17"
projects[ckeditor][subdir] = "contrib"

projects[views][version] = "3.13"
projects[views][subdir] = "contrib"

projects[views_responsive_grid][version] = "1.3"
projects[views_responsive_grid][subdir] = "contrib"

; +++++ Themes +++++

; zen
projects[zen][type] = "theme"
projects[zen][version] = "5.6"
projects[zen][subdir] = "contrib"

; +++++ Libraries +++++

; jQuery Superfish
libraries[superfish][directory_name] = "superfish"
libraries[superfish][type] = "library"
libraries[superfish][destination] = "libraries"
libraries[superfish][download][type] = "get"
libraries[superfish][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal/archive/master.zip"

