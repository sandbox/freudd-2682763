CONTENU DE CE DOSSIER

---------------------

�

 * Introduction
�
 * Exigences
�
 * modules recommand�s
�
 * Installation
�
 * Configuration
�
 * Maintainers



INTRODUCTION

------------



OpenEPortfolio vous permettra de cr�er un site personnel en partant d'une base solide.

Un ePortfolio ou Porffolio num�rique est une fa�on d�organiser ses comp�tences, ses exp�riences, 
ses projets, ses centres d�int�r�ts afin de laisser entrevoir vos valeurs. Il est r�guli�rement 
utile pour des raisons professionnelles d�y afficher vos comp�tences r�elles.

En termes d�apprentissage web je vous conseille personnellement de cr�er votre premier 
e-portfolio � la main. Vous pourrez acqu�rir de nombreuses connaissances dans le web 
(XHTML � HTML, CSS, Javascript, JQuery, PHP, MySQL, MariaSQL, SQL �) Cependant avec le temps 
votre contenu s��largira et vous devrez envisager une solution plus efficace sur le long terme 
pour �viter de r��crire les fondements de votre e-portfolio. OpenEPortfolio vous propose 
cette solution.

�

  * Pour une description compl�te du module, visitez la page du projet:
    https://www.drupal.org/sandbox/freudd/2682763

�

  * Pour soumettre des rapports de bogues et disposent des suggestions, ou pour suivre les changements:
    
https://www.drupal.org/project/issues/2682763



EXIGENCES

------------



Cette distribution requiert les modules de contribution suivants:


  * Ctools 		v 1.9 (https://www.drupal.org/project/ctools)
  * Date		v 2.9 (https://www.drupal.org/project/date)
  * Features		v 2.7 (https://www.drupal.org/project/features)
  * UUID Features	v 1.0-alpha4 (https://www.drupal.org/project/uuid_features)
  * Link		v 1.4 (https://www.drupal.org/project/link)
  * IMCE		v 1.9 (https://www.drupal.org/project/imce)
  * Localization update v 1.1 (https://www.drupal.org/project/l10n_update)
  * Entity API		v 1.6 (https://www.drupal.org/project/entity)
  * Libraries API	v 2.2 (https://www.drupal.org/project/libraries)
  * Menu attributes	v 1.0 (https://www.drupal.org/project/menu_attributes)
  * Pathauto		v 1.3 (https://www.drupal.org/project/pathauto)
  * Token		v 1.6 (https://www.drupal.org/project/token)
  * Transliteration	v 3.2 (https://www.drupal.org/project/transliteration)
  * Panel		v 3.5 (https://drupal.org/project/panels)
  * Global Redirect	v 1.5 (https://www.drupal.org/project/globalredirect)
  * Metatag		v 1.13 (https://www.drupal.org/project/metatag)
  * CAPTCHA		v 1.3 (https://www.drupal.org/project/captcha)
  * Universally Unique Identifier v 1.0-beta1 (https://www.drupal.org/project/uuid)
  * CKEditor - WYSIWYG HTML editor v 1.17 (https://www.drupal.org/project/ckeditor)
  * Views		v 3.13 (https://drupal.org/project/views)
  * View Responsive Grid v 1.3 (https://www.drupal.org/project/views_responsive_grid)

Cette distribution requiert les th�mes de contribution suivants�:
  * Zen			v 5.6 (https://www.drupal.org/project/zen)

Cette distribution requiert les libraires de contribution suivants:
  * Superfish-for-Drupal (https://github.com/mehrpadin/Superfish-for-Drupal/)



MODULES RECOMMAND�ES

-------------------

�

  * Display Suite (https://www.drupal.org/project/ds)
    Si vous souhaitez personnaliser vos contenus sans toucher � la moindre ligne de code je vous conseille ce module.
  * jQuery Update (https://www.drupal.org/project/jquery_update)
    Si vous souhaitez modifier la version de Jquery je vous conseille ce module.
  * Webform (https://www.drupal.org/project/webform)
    Si vous souhaitez cr�er des formulaires complexes je vous conseille ce module.

INSTALLATION

------------

�

  * Installez comme vous le feriez normalement installer un site Drupal.
    Voir�: https://www.drupal.org/documentation/install pour plus d�informations.

CONFIGURATION

-------------


Pour commencer modifier les informations g�n�rales du site�:

   - le ��nom du site��. (limit� � 128 caract�res)

   - le ��slogan du site��. (limit� � 128 caract�res)

Attention�!
Je vous conseille tr�s fortement de mettre ��votre pr�nom nom�� pour le ��nom du site��. 
Le slogan du site influencera grandement le r�f�rencement de votre e-portfolio. Il est 
utilis� dans la description de votre site ainsi que dans le titre des pages. Pensez donc 
� vous d�crire soigneusement en choisissant des mots cl�s. Ces deux �l�ments seront 
affich�s dans les r�sultats des moteurs de recherches.

  * Les URLs de votre site

Les URL de votre site permettent d�influencer sur le r�f�rencement et pr�senter vos pages 
de fa�on claire et transparente.

   - Activez les URL propres,

   - supprimez les anciens Alias d�URL,

   - g�n�rez � nouveau les Alias d�URL.

Des patterns sont install�s de base, libre � vous de les modifier � votre guise.

  * Configurer votre th�me

Le th�me par d�faut d�OpenEPortfolio est ��Eotyos��. Il a �t� r�alis� � partir d�un STARTERKIT
du th�me Zen vous garantissant une structure d�affichage responsive et stable � partir de IE8.
Il utilise �galement la grille du Framework Bootstrap 3.

   - Activer le th�me Eotyos par d�faut si ce n�est d�j� fait,

   - modifier le logo par d�faut par une photo de profil,

   - modifier la favicon par d�faut,

le fil d�Ariane est d�sactiv� par d�faut, un e-portfolio en format ��one page�� n�a pas besoin 
d�un fil d�Ariane. Libre � vous de l�activer, les autres param�tres de ��Accessibility and support settings�� 
permettent d�ajouter le script respond.js pour utiliser les propri�t�s basiques comme 
les ��media query�� pour les navigateurs IE 6-8. Il ajoute �galement les balises m�ta sp�cifiant que 
votre site peut-�tre affich� sur mobile et tablette.

  * Ajoutez du contenu

� l�installation d�OpenEPortfolio, les modules de d�mo ajoutent du contenu de pr�sentation. Ces 
contenus sont fictifs. Ils doivent �tre supprim�s pour faire place aux v�tres.

   - D�sactiver tous les modules de d�mo,

   - supprimer les contenus.

   - Ajoutez vos contenus

   - Education�: Ajoutez vos dipl�mes (Exemple : BAC, Licence, Master ...).

   - Experience�: Ajoutez vos exp�riences professionnelles (Exemple : Stage, Travail saisonnier, CDD, CDI ...).

   - Interest�: Ajoutez vos centres d�int�r�ts personnels ou professionnels 
     (Exemple : Sport, Musique, Association, Film, B�n�volat ...).
   
   - Project�: Ajoutez vos projets (personnels, professionnels, universitaires �).

   - Page : Permet d�ajouter une simple page (non obligatoire).

   -Article : Permet d�ajouter un billet de blog (non obligatoire).

 * Comp�tences

Vos comp�tences vous permettront de mettre en valeur votre savoir faire. Il existe 2 utilisations 
pour g�rer vos comp�tences�:

   1. Premi�re utilisation�: Choisir d�ajouter vos comp�tences dans les contenus (voir �tape 4), 
      vos comp�tences seront comptabilis�es et s�afficheront dans le format d�un nuage de mot. 
      Ainsi le nuage de mot sera repr�sentatif de vos comp�tences acquises dans vos exp�riences, 
      projets, dipl�mes et centre d�int�r�ts.
   
   2. Seconde utilisation�: Ajouter simplement vos comp�tences, hi�rarchisez-les et ajoutez un 
      pourcentage repr�sentatif de votre savoir faire.

Exemple�:
��Microsoft office� de 80�%,
��Gestion de projet�� � 70�%.

La gestion des comp�tences par la taxonomie permet �galement de rassembler vos contenus contenant le m�me terme.

  * Choisir ses affichages

L�affichage de la page principale est cr�� gr�ce � 2 modules�: ��Panel�� et ��Views��.
Le module ��Panel�� permet de cr�er une page avec de nombreuses fonctionnalit�s. 
Son point fort r�side dans sa gestion de contenus. Il peut inclure des contenus, 
des blocs personnalis�s, des blocs syst�mes, des vues�
Le module ��Views�� est l��quivalent d�une requ�te SQL avec une interface d�administrateur. 
Elle s�lectionne le contenu, choisit son affichage, filtre et trie les contenus�

   -Rendez-vous dans l�espace de configuration des pages,

   -Modifiez la page ��home��,

   -Allez dans l�onglet ��contenu�� de la variante ��one_page-simple��

   -Ajoutez, modifiez, supprimez les blocs � votre guise.

   -Les modules OpenEPortfolio Education, OpenEPortfolio Experience, OpenEPortfolio Interset, 
    OpenEPortfolio Project et OpenEPortfolio Skills embarquent des vues pr�tes � l�emploi. � vous de 
    choisir lesquels vous correspondent le mieux pour votre e-portfolio.

Par d�faut la page est configur�e avec le contenu suivant�:

   -Block personnalis� ��Resume��

   -Views�: Eportfolio Experience�: Block-3col-fluid

   -Views�: Eportfolio Project: Block-4grid

   -Views�: Eportfolio Skills cloud: Block-sort-count

   -Views�: Eportfolio Skills: Block-sort-perc-parent

   -Views�: Eportfolio Education: Block-list

   -Views�: Eportfolio Interst: Block-2col-fluid

   -Block system ��Contact form��

Si un affichage de vue ne vous convient pas, rendez-vous dans l�interface d�administration des vues, 
cloner celle que vous souhaitez modifier pour pouvoir conserver la vue originale.



MAINTAINERS

-----------



mainteneurs actuels:
* Mathieu No�l (Freudd) - https://www.drupal.org/user/3233559
