<?php
/**
 * @file
 * openeportfolio_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function openeportfolio_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: fit_large.
  $styles['fit_large'] = array(
    'label' => 'Fit Large (600x375)',
    'effects' => array(
      15 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 375,
        ),
        'weight' => 4,
      ),
    ),
  );

  // Exported image style: fit_medium.
  $styles['fit_medium'] = array(
    'label' => 'Fit Medium (400x250)',
    'effects' => array(
      16 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 250,
        ),
        'weight' => 4,
      ),
    ),
  );

  // Exported image style: fit_thumbnail.
  $styles['fit_thumbnail'] = array(
    'label' => 'Fit Thumbnail (200x125)',
    'effects' => array(
      17 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 125,
        ),
        'weight' => 4,
      ),
    ),
  );

  return $styles;
}
