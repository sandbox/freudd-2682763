<?php
/**
 * @file
 * openeportfolio_education.features.inc
 */

/**
 * Implements hook_views_api().
 */
function openeportfolio_education_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function openeportfolio_education_node_info() {
  $items = array(
    'education' => array(
      'name' => t('Education'),
      'base' => 'node_content',
      'description' => t('Ajoutez vos diplômes (<i>Exemple : BAC, Licence, Master ...</i>).'),
      'has_title' => '1',
      'title_label' => t('Diplôme'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
