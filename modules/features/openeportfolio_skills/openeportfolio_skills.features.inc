<?php
/**
 * @file
 * openeportfolio_skills.features.inc
 */

/**
 * Implements hook_views_api().
 */
function openeportfolio_skills_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
