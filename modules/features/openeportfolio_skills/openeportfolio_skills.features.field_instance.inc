<?php
/**
 * @file
 * openeportfolio_skills.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function openeportfolio_skills_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-skills-field_percentage'.
  $field_instances['taxonomy_term-skills-field_percentage'] = array(
    'bundle' => 'skills',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => 1,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_percentage',
    'label' => 'Pourcentage',
    'required' => 0,
    'settings' => array(
      'max' => 100,
      'min' => 0,
      'prefix' => '',
      'suffix' => '%',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Pourcentage');

  return $field_instances;
}
