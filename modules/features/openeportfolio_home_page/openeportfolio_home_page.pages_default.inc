<?php
/**
 * @file
 * openeportfolio_home_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function openeportfolio_home_page_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home';
  $page->task = 'page';
  $page->admin_title = 'Home';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home__panel_context_9fbe6fa6-7b3e-4bc1-9094-e1158a8adc74';
  $handler->task = 'page';
  $handler->subtask = 'home';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'one_page-simple',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'one-page',
    'css_id' => 'simple',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '51f01734-79bd-4c3f-9605-bf15f43b39f8';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-940d7181-0be8-4e0d-b99d-0dcb9dcfeedf';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'resume',
      'title' => 'Resume',
      'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam turpis tincidunt risus dignissim aliquam. Maecenas non sem justo. Duis at ex neque. Nunc augue risus, posuere feugiat enim ut, vehicula rutrum quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras tincidunt vel ligula ac iaculis.</p>

<p>Nam dictum, ipsum rhoncus bibendum blandit, justo libero cursus ante, eu commodo mi enim non nisl. Praesent rhoncus sed ex a vestibulum. Aliquam ullamcorper convallis congue. Pellentesque ac pellentesque tellus. Morbi vel ante vitae purus fringilla feugiat id vitae leo. Mauris eu ante quis eros molestie commodo sed vel neque.</p>

<p>Mauris lorem leo, bibendum ac venenatis aliquam, sollicitudin quis arcu. Duis sem erat, euismod id gravida ac, lobortis in enim. Aenean faucibus est felis, sed imperdiet neque ullamcorper quis. Aliquam erat volutpat. Pellentesque congue luctus rhoncus. Vivamus congue diam sit amet sagittis tempor.</p>
',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'home',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '940d7181-0be8-4e0d-b99d-0dcb9dcfeedf';
    $display->content['new-940d7181-0be8-4e0d-b99d-0dcb9dcfeedf'] = $pane;
    $display->panels['middle'][0] = 'new-940d7181-0be8-4e0d-b99d-0dcb9dcfeedf';
    $pane = new stdClass();
    $pane->pid = 'new-80ad61dd-113c-46db-8b61-15fd65c81a26';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'eportfolio_experience';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'experience',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '80ad61dd-113c-46db-8b61-15fd65c81a26';
    $display->content['new-80ad61dd-113c-46db-8b61-15fd65c81a26'] = $pane;
    $display->panels['middle'][1] = 'new-80ad61dd-113c-46db-8b61-15fd65c81a26';
    $pane = new stdClass();
    $pane->pid = 'new-6f27c00a-7554-4bf8-a70b-bba6bec08691';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'eportfolio_project_grid';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'project',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '6f27c00a-7554-4bf8-a70b-bba6bec08691';
    $display->content['new-6f27c00a-7554-4bf8-a70b-bba6bec08691'] = $pane;
    $display->panels['middle'][2] = 'new-6f27c00a-7554-4bf8-a70b-bba6bec08691';
    $pane = new stdClass();
    $pane->pid = 'new-1809eb8b-6e8d-48a0-a290-53aa555bca79';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'eportfolio_skills_cloud';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'skills',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '1809eb8b-6e8d-48a0-a290-53aa555bca79';
    $display->content['new-1809eb8b-6e8d-48a0-a290-53aa555bca79'] = $pane;
    $display->panels['middle'][3] = 'new-1809eb8b-6e8d-48a0-a290-53aa555bca79';
    $pane = new stdClass();
    $pane->pid = 'new-8543380b-2c06-443c-ac96-6621ae9142e2';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'eportfolio_skills';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_3',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '8543380b-2c06-443c-ac96-6621ae9142e2';
    $display->content['new-8543380b-2c06-443c-ac96-6621ae9142e2'] = $pane;
    $display->panels['middle'][4] = 'new-8543380b-2c06-443c-ac96-6621ae9142e2';
    $pane = new stdClass();
    $pane->pid = 'new-c1d6ae2e-f024-41a8-8353-3f4f4eca8fba';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'eportfolio_education';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_2',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'education',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'c1d6ae2e-f024-41a8-8353-3f4f4eca8fba';
    $display->content['new-c1d6ae2e-f024-41a8-8353-3f4f4eca8fba'] = $pane;
    $display->panels['middle'][5] = 'new-c1d6ae2e-f024-41a8-8353-3f4f4eca8fba';
    $pane = new stdClass();
    $pane->pid = 'new-572feef2-29e5-4f88-9f63-3590be265a96';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'eportfolio_interest';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '6',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'interest',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '572feef2-29e5-4f88-9f63-3590be265a96';
    $display->content['new-572feef2-29e5-4f88-9f63-3590be265a96'] = $pane;
    $display->panels['middle'][6] = 'new-572feef2-29e5-4f88-9f63-3590be265a96';
    $pane = new stdClass();
    $pane->pid = 'new-2d372e1f-c48a-48cf-b898-1a61d830a8b0';
    $pane->panel = 'middle';
    $pane->type = 'contact';
    $pane->subtype = 'contact';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Contact',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'contact',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = '2d372e1f-c48a-48cf-b898-1a61d830a8b0';
    $display->content['new-2d372e1f-c48a-48cf-b898-1a61d830a8b0'] = $pane;
    $display->panels['middle'][7] = 'new-2d372e1f-c48a-48cf-b898-1a61d830a8b0';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home'] = $page;

  return $pages;

}
